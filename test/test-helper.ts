import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import "mocha";
import sinon = require("sinon");
import sinonChai = require("sinon-chai");

chai.use(sinonChai);
chai.use(chaiAsPromised);

export const expect = chai.expect;
export let sandbox = sinon.createSandbox();

before(() => {
  sandbox = sinon.createSandbox();
});

afterEach(() => {
  if (!!sandbox) {
    sandbox.restore();
    sandbox.resetHistory();
  }

  sinon.restore();
  sinon.resetHistory();
});
