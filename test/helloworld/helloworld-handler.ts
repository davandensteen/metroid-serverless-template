import { handler } from "../../src/helloworld/helloworld-handler";
import { expect, sandbox } from "../test-helper";

describe("helloworld-handler", () => {
  it("should exist", async () => {
    expect(handler).to.exist;
  });

  it("should also work for promises", async () => {
    expect(handler).to.eventually.exist;
  });

  afterEach(() => {
    sandbox.restore();
  });
});
