# metroid-serverless-template

API to be used by ...
Setup with the Serverless Framework (https://serverless.com/)

### Environments

- DEV: https://kubus-service-dev.rubik.persgroep.cloud/???/
- PROD: https://kubus-service.api.persgroep.cloud/???/

#### Installing serverless

```
npm install -g serverless
```

#### Configure AWS CLI credentials

https://serverless.com/framework/docs/providers/aws/guide/credentials/

If you have different aws-cli profiles configured,
you can always specify the profile which should be used via the aws-profile option like this:

```
sls deploy --aws-profile rubik
```

#### How to deploy?

DEV:

```
sls deploy
```
