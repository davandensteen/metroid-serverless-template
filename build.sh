#!/bin/bash
set -e
set -o pipefail

instruction()
{
  echo "usage: ./build.sh deploy <env>"
  echo ""
  echo "env: eg. dev, prod, ..."
  echo ""
  echo "for example: ./deploy.sh dev"
}

if [ $# -eq 0 ]; then
  instruction
  exit 1

elif [ "$1" = "test" ] && [ $# -eq 1 ]; then
    npm install
    if ! [ -d .out/ ]; then
    mkdir .out
    fi
    if ! [ -d .out/test-results ]; then
    mkdir .out/test-results
    fi
    npm run cilint
    npm run citest

elif [ "$1" = "package" ] && [ $# -eq 1 ]; then
    npm install
    'node_modules/.bin/sls' package

elif [ "$1" = "deploy" ] && [ $# -eq 2 ]; then
    STAGE=$2
    npm install
    'node_modules/.bin/sls' deploy -s ${STAGE}

elif [ "$1" = "bitbucket_setup" ] && [ $# -eq 1 ]; then
    if [ -z ${SLS_ACCESS_KEY} ] || [ -z ${SLS_ACCESS_SECRET} ]; then
      echo "Missing ENV variables: SLS_ACCESS_SECRET or SLS_ACCESS_SECRET"
      exit 1
    else
       export AWS_ACCESS_KEY_ID=${SLS_ACCESS_KEY}
       export AWS_SECRET_ACCESS_KEY=${SLS_ACCESS_SECRET}
    fi

else
    instruction
    exit 1
fi